#!/usr/bin/env python3

#
# Copyright (c) 2023 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import logging
import os
import re
import subprocess
import sys
import time

import glob

from prometheus_client import Gauge, start_http_server


class NoOVSSocketError(Exception):
    pass


FLOWLIMIT_RE = re.compile(r"limit \d+")
FLOWCURRENT_RE = re.compile(r"current \d+")

LABEL = ["chassis_name"]
CONN_LABEL = ["connection_type"]

SOCKET_UP_GAUGE = Gauge(
    "socket_up", "Whether the vswitchd socket is up", LABEL)

FLOW_LIMIT_GAUGE = Gauge("flow_limit", "Flow limit of the vswitchd", LABEL)

FLOW_CURRENT_GAUGE = Gauge(
    "flow_current", "Current flows of the vswitchd", LABEL
)

DUMP_DURATION_GAUGE = Gauge(
    "dump_duration", "Dump duration of the vswitchd", LABEL
)


def _get_prometheus_port():
    return int(os.getenv("PROMETHEUS_ENDPOINT_HTTP_SERVER_PORT", "9090"))


def _get_scrape_interval():
    return int(os.getenv("SCRAPE_INTERVAL", "30"))


def find_vswitchd_control_socket():
    """
    Check if environment for scraping metrics is given.
    """
    logging.basicConfig(level=logging.INFO)

    vswitchd_control_socket_candidates = glob.glob(
        "/run/openvswitch/ovs-vswitchd.*.ctl"
    )

    if not vswitchd_control_socket_candidates:
        raise NoOVSSocketError

    return vswitchd_control_socket_candidates[0]


def update_status(socket_up: bool) -> None:
    hostname = os.environ["HOSTNAME"]
    SOCKET_UP_GAUGE.labels(chassis_name=hostname).set(int(socket_up))


def update(vswitchd_control_socket: str):
    """
    Connect to the ovsdb server on the node using ovsdb-client
    and update metrics.
    """
    hostname = os.environ["HOSTNAME"]
    cmd_upcall = ["ovs-appctl", "-t", vswitchd_control_socket, "upcall/show"]
    result = subprocess.run(
        cmd_upcall, capture_output=True, check=True,
    )
    update_status(True)
    lines = [
        line.strip()
        for line in result.stdout.decode().splitlines()
    ]
    del lines[0]
    del lines[-1]
    for line in lines:
        if line.startswith("dump"):
            dump_duration = line.split(":")[1]
            DUMP_DURATION_GAUGE.labels(chassis_name=hostname).set(
                int(dump_duration.strip("ms"))
            )
        if line.startswith("flows"):
            flow_limit_match = FLOWLIMIT_RE.findall(line)[0]
            flow_limit = flow_limit_match.split()[1]
            FLOW_LIMIT_GAUGE.labels(chassis_name=hostname).set(
                int(flow_limit)
            )
            flow_current_match = FLOWCURRENT_RE.findall(line)[0]
            flow_current = flow_current_match.split()[1]
            FLOW_CURRENT_GAUGE.labels(chassis_name=hostname).set(
                int(flow_current)
            )


def _reset() -> None:
    DUMP_DURATION_GAUGE.clear()
    FLOW_LIMIT_GAUGE.clear()
    FLOW_CURRENT_GAUGE.clear()


def _reset_status() -> None:
    SOCKET_UP_GAUGE.clear()


def main():
    """
    Start the prometheus endpoint to serve the metric.
    Every second update the metric by reading from the local ovsdb server.
    """

    scrape_interval = _get_scrape_interval()

    start_http_server(port=_get_prometheus_port())

    _reset()
    while True:
        try:
            vswitchd_control_socket = find_vswitchd_control_socket()
            update(vswitchd_control_socket)
        except (NoOVSSocketError, subprocess.CalledProcessError):
            logging.error(
                "could not retrieve metrics from the ovs-vswitchd socket")
            _reset()
            update_status(False)
        except Exception as e:
            logging.error(e)
            _reset()
            # a non expected error, we can't even tell whether the socket is up
            _reset_status()
        time.sleep(scrape_interval)


if __name__ == "__main__":
    sys.exit(main())
