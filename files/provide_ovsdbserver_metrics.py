#!/usr/bin/env python3

#
# Copyright (c) 2022 The Yaook Authors.
#
# This file is part of Yaook.
# See https://yaook.cloud for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""
Welcome to the check script for nb_cfg values of south- and northbound servers.
This script heavily depends on the ovsdb-server statefulset yaml because it
re-uses most of the environment variables from it to connect and read the db.

Note that even though the metric itself is called nb_cfg the value itself may
come from the northbound database OR the southbound database.
"""

import json
import logging
import os
import subprocess
import sys
import time
import typing
import socket

from ovsdbapp.backend.ovs_idl import connection
from ovsdbapp.schema.ovn_northbound import impl_idl as idl_nb
from ovsdbapp.schema.ovn_southbound import impl_idl as idl_sb

from prometheus_client import Gauge, start_http_server

LABELS = ["dbname", "table"]
CHASSIS_LABELS = ["chassis_name"]
LRP_LABELS = ["lrp"]
PRIORITY_LABELS = ["priority"]

NB_CFG_GAUGE = Gauge(
    "nb_cfg", "Shows the nb_cfg value from the global table.", LABELS
)


SB_CFG_GAUGE = Gauge(
    "sb_cfg", "Shows the sb_cfg value from the global table.", LABELS
)


# only leader has election time information
ELECTION_START_GAUGE = Gauge(
    "ovsdb_cluster_election_start",
    "Time since ovsdb election was started in ms.",
    LABELS,
)


ELECTION_WON_GAUGE = Gauge(
    "ovsdb_cluster_election_won",
    "Time since ovsdb election was won in ms.",
    LABELS,
)


CLUSTER_ROLE_GAUGE = Gauge(
    "ovsdb_cluster_role",
    "Shows which role the cluster member has. "
    "1 = leader, 2 = follower, 0 = no role/unknown",
    LABELS,
)


CLUSTER_TERM_GAUGE = Gauge(
    "ovsdb_cluster_term", "Shows term value of cluster/status.", LABELS
)


CLUSTER_NOT_COMMITTED_GAUGE = Gauge(
    "ovsdb_cluster_not_committed_entries",
    "Shows number of not committed entries.",
    LABELS,
)


CLUSTER_NOT_APPLIED_GAUGE = Gauge(
    "ovsdb_cluster_not_applied_entries",
    "Shows number of not applied entries.",
    LABELS,
)


SB_CHASSIS_PRIVATE_GAUGE_NB_CFG = Gauge(
    "sb_chassis_private_nb_cfg",
    "Shows nb_cfg for chassis in chassis_private table",
    LABELS + CHASSIS_LABELS,
)

SB_CHASSIS_PRIVATE_GAUGE_NB_CFG_TIMESTAMP = Gauge(
    "sb_chassis_private_nb_cfg_timestamp",
    "Show nb_cfg_timestamp for chassis in chassis_private table",
    LABELS + CHASSIS_LABELS,
)

GATEWAY_CHASSIS_PRIORITY_GAUGE = Gauge(
    "gateway_chassis_priority",
    "Priority of gateway chassis",
    LABELS + CHASSIS_LABELS + LRP_LABELS,
)

GATEWAY_CHASSIS_PRIORITY_COUNT_GAUGE = Gauge(
    "gateway_chassis_priority_count",
    "Count of LRP priorities on gateway chassis",
    LABELS + CHASSIS_LABELS + PRIORITY_LABELS,
)


def _get_prometheus_port():
    return int(os.getenv("PROMETHEUS_ENDPOINT_HTTP_SERVER_PORT", "9090"))


def _get_scrape_interval():
    return int(os.getenv("SCRAPE_INTERVAL", "30"))


def _execute_cmd(cmd: typing.List[str]):
    with subprocess.Popen(
        cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE
    ) as process:
        if process.stdout is None:
            return
        lines = [
            x.strip()
            for x in typing.cast(
                typing.IO[bytes],
                process.stdout,
            )
            .read()
            .decode()
            .split("\n")
        ]
    return lines


def preflight(dbsock: str):
    """
    Check for environment variables and check if the socket exists.
    """
    logging.basicConfig(level=logging.INFO)

    for required_env_var in [
        "DBCTL",
        "DBSOCK",
        "DBNAME",
        "GLOBAL",
    ]:
        if required_env_var not in os.environ:
            logging.error(f"Environment var [{required_env_var}] not found.")
            sys.exit(1)

    if not os.path.exists(str.replace(dbsock, "unix:/", "/")):
        logging.error(f"Socket file [{dbsock}] does not exist.")
        sys.exit(1)


def _get_db_type() -> str:
    pod_name = socket.gethostname()
    parts = pod_name.split("-")
    return parts[1]


def _get_statefulset_index() -> int:
    pod_name = socket.gethostname()
    parts = pod_name.split("-")
    return int(parts[-1])


def init_ovsdb_connection() -> typing.Any:
    conn = os.getenv("OVSDB_SOCKET", "tcp:127.0.0.1:6632")

    try:
        if _get_db_type() == "northbound":
            i = connection.OvsdbIdl.from_server(
                conn, "OVN_Northbound", leader_only=False
            )
            c = connection.Connection(idl=i, timeout=3)
            api = idl_nb.OvnNbApiIdlImpl(c)
        else:
            i = connection.OvsdbIdl.from_server(
                conn, "OVN_Southbound", leader_only=False
            )
            c = connection.Connection(idl=i, timeout=3)
            api = idl_sb.OvnSbApiIdlImpl(c)

        return api
    except Exception as e:
        logging.error(e)
    return None


def update(dbsock: str, api: typing.Any, ovsdb_schema: str):
    """
    Connect to the ovsdb server using ovsdb-client and update the metric.
    """
    dbctl = os.environ["DBCTL"]
    dbname = os.environ["DBNAME"]
    table = os.environ["GLOBAL"]

    query = [dbname, {"op": "select", "table": table, "where": []}]

    for result in json.loads(
        subprocess.check_output(
            ["ovsdb-client", "transact", dbsock, json.dumps(query)]
        )
    ):
        for row in result["rows"]:
            NB_CFG_GAUGE.labels(dbname=dbname, table=table).set(
                int(row["nb_cfg"])
            )
            if table == "NB_Global":
                SB_CFG_GAUGE.labels(dbname=dbname, table=table).set(
                    int(row["sb_cfg"])
                )

    # get raft/cluster information
    cmd = ["ovs-appctl", "-t", dbctl, "cluster/status", dbname]
    lines = _execute_cmd(cmd)
    if lines is None:
        return
    election_start = int()
    election_won = int()
    # Set worst case values as "default" so we see, if we can't get this
    # metrics from cluster/status.
    role_code = 0
    not_committed_entries = -1
    not_applied_entries = -1
    for line in lines:
        # try to get election information
        if line.startswith("Last Election started"):
            election_start = int(line.split(" ")[3])
        elif line.startswith("Last Election won:"):
            election_won = int(line.split(" ")[3])
        # get other cluster infromation
        elif line.startswith("Role:"):
            role = line.split(" ")[1]
            if role == "leader":
                role_code = 1
            elif role == "follower":
                role_code = 2
        elif line.startswith("Term:"):
            term = int(line.split(" ")[1])
        elif line.startswith("Entries not yet committed:"):
            not_committed_entries = int(line.split(" ")[4])
        elif line.startswith("Entries not yet applied:"):
            not_applied_entries = int(line.split(" ")[4])
    # we assume, both, election_start and won are set, as only the leader
    # has both lines.
    if election_start:
        ELECTION_START_GAUGE.labels(dbname=dbname, table=table).set(
            election_start
        )
        ELECTION_WON_GAUGE.labels(dbname=dbname, table=table).set(election_won)
    CLUSTER_ROLE_GAUGE.labels(dbname=dbname, table=table).set(role_code)
    CLUSTER_TERM_GAUGE.labels(dbname=dbname, table=table).set(term)
    CLUSTER_NOT_COMMITTED_GAUGE.labels(dbname=dbname, table=table).set(
        not_committed_entries
    )
    CLUSTER_NOT_APPLIED_GAUGE.labels(dbname=dbname, table=table).set(
        not_applied_entries
    )

    if _get_statefulset_index() != 0:
        return

    if api is None:
        return
    if ovsdb_schema == "northbound":
        priority_distribution: typing.Dict[str, typing.Dict[str, int]] = {}
        priorities: typing.Set[str] = set()
        for chassis in api.db_find(
            "Gateway_Chassis", columns=["chassis_name", "priority", "name"]
        ).execute(check_error=True):
            chassis_name = chassis["chassis_name"]
            chassis_priority = chassis["priority"]
            lrp_name = chassis["name"].split("_")[0]
            GATEWAY_CHASSIS_PRIORITY_GAUGE.labels(
                dbname=dbname,
                table="Gateway_Chassis",
                chassis_name=chassis_name,
                lrp=lrp_name,
            ).set(int(chassis_priority))
            if chassis_name not in priority_distribution:
                priority_distribution[chassis_name] = {}
            if chassis_priority not in priority_distribution[chassis_name]:
                priority_distribution[chassis_name][chassis_priority] = 0
            priority_distribution[chassis_name][chassis_priority] += 1
        for chassis, priority_dict in priority_distribution.items():
            for priority, priority_count in priority_dict.items():
                GATEWAY_CHASSIS_PRIORITY_COUNT_GAUGE.labels(
                    dbname=dbname,
                    table="Gateway_Chassis",
                    chassis_name=chassis,
                    priority=priority,
                ).set(priority_count)
            priorities_on_chassis = set(priority_dict.keys())
            priority_difference = priorities.difference(priorities_on_chassis)
            for priority in priority_difference:
                GATEWAY_CHASSIS_PRIORITY_COUNT_GAUGE.labels(
                    dbname=dbname,
                    table=table,
                    chassis_name=chassis,
                    priority=priority,
                ).set(0)
    else:
        for chassis in api.db_find("Chassis_Private").execute(
            check_error=True
        ):
            chassis_name = chassis["name"]
            chassis_nfg = int(chassis["nb_cfg"])
            chassis_nfg_timestamp = int(chassis["nb_cfg_timestamp"])
            SB_CHASSIS_PRIVATE_GAUGE_NB_CFG.labels(
                dbname=dbname, table=table, chassis_name=chassis_name
            ).set(chassis_nfg)
            SB_CHASSIS_PRIVATE_GAUGE_NB_CFG_TIMESTAMP.labels(
                dbname=dbname,
                table="Chassis_Private",
                chassis_name=chassis_name,
            ).set(chassis_nfg_timestamp)


def _reset() -> None:
    SB_CHASSIS_PRIVATE_GAUGE_NB_CFG.clear()
    SB_CHASSIS_PRIVATE_GAUGE_NB_CFG_TIMESTAMP.clear()
    GATEWAY_CHASSIS_PRIORITY_GAUGE.clear()
    GATEWAY_CHASSIS_PRIORITY_COUNT_GAUGE.clear()


def _reset_on_error() -> None:
    NB_CFG_GAUGE.clear()
    SB_CFG_GAUGE.clear()
    ELECTION_START_GAUGE.clear()
    ELECTION_WON_GAUGE.clear()
    CLUSTER_ROLE_GAUGE.clear()
    CLUSTER_TERM_GAUGE.clear()
    CLUSTER_NOT_COMMITTED_GAUGE.clear()
    CLUSTER_NOT_APPLIED_GAUGE.clear()
    SB_CHASSIS_PRIVATE_GAUGE_NB_CFG.clear()
    SB_CHASSIS_PRIVATE_GAUGE_NB_CFG_TIMESTAMP.clear()
    GATEWAY_CHASSIS_PRIORITY_GAUGE.clear()
    GATEWAY_CHASSIS_PRIORITY_COUNT_GAUGE.clear()


def main():
    """
    Start the prometheus endpoint to serve the metric.
    Every second update the metric by reading from the ovsdb.
    Note that cleanup of metrics is not required here.
    """
    dbsock = str.replace(os.environ["DBSOCK"], "punix:/", "unix:/")
    scrape_interval = _get_scrape_interval()

    preflight(dbsock)

    ovsdb_api = None
    ovsdb_schema = _get_db_type()

    start_http_server(port=_get_prometheus_port())

    _reset()

    while True:
        try:
            if ovsdb_api is None and _get_statefulset_index() == 0:
                ovsdb_api = init_ovsdb_connection()
            update(dbsock, ovsdb_api, ovsdb_schema)
        except Exception as e:
            logging.error(e)
            _reset_on_error()
        time.sleep(scrape_interval)
        _reset()


if __name__ == "__main__":
    sys.exit(main())
